# Generated by Django 4.2.3 on 2023-07-26 08:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0011_payment_payment_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='is_paid',
            field=models.BooleanField(default=False, verbose_name='payment_status'),
        ),
    ]
